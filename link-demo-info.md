# CDR Link: Frequently Asked Questions

## What is CDR Link?

CDR Link is an application ecosystem comprised of a number of open source tools that address a number of current challenges facing the civil society incident responder community. Its current and future components include:

* **Zammad helpdesk**: A customized and hardened version of Zammad, an open source, flexible, multi-channel ticketing platform that enables those in need to ask for help, and providers to respond.

* **Custom settings**: CDR Link adjusts Zammad’s defaults to ensure a secure hosting setup

* **Multi-channel integrations**: Integrations with popular messaging apps, including Signal, WhatsApp, Telegram, and Twitter, making it easily to securely reach responders to request assistance  

* **Security**: Secure methods of ticket creation and communication that don’t require users to install complex and difficult software like PGP

* **MISP**: Open source platform incorporating data from CDR's Zammad instance and from other community partners, enabling information sharing of threat intelligence including cyber security indicators.

* **Leafcutter**: A dashboard integrating with MISP to enable CDR and our partners to display and analyze threat intelligence data.

* **Documentation**: Detailed, plain-text documentation of every inch of the software stack, including support for self hosting, resources for digital security trainers, and resources from the wider community

## What is CDR?

[Center for Digital Resilience](https://digiresilience.org) is a non-governmental organization (NGO) that partners with communities around the world to provide sustainable, strategic approaches to long-term digital wellness. We see security as a collective community endeavour rather than solely an individual or organisational one. We work with regional and thematic communities of Civil Society Organisations (CSOs) around the world to improve their collective understanding of the threats they face and how to mitigate them. We aim to not only ensure timely and effective responses for our communities, but to reduce their need for emergency response in the first place. The impact of our work is to protect civil society and empower activists to regain civic space.

 We work alongside partners and a network of trusted digital security responders to provide security assessments, preventative wellness plans, trainings, incident response, alerts about crucial threats, and ongoing support. We currently operate one program in the Middle East/North Africa region and one program, [the Digital Security Exchange](https://dsx.us), in North America.

## What are CDR Link's values?

CDR Link is guided by the community-minded, open source, sustainability-focused ethic of CDR's programmatic work. These principles include:

* **Security**: Security and privacy are our first priority, and must be included from the start, never an afterthought or add-on.

* **Interoperability**: Link’s technology, taxonomy, processes, and frameworks must interoperate with other platforms in our space.

* **Open source**: All technology that we create or adapt must be open source, ideally licensed under the GPL, and we build on the shoulders of giants by incorporating trusted, open-source platforms and technologies.

* **Unix philosophy**: Many small parts doing single things well, rather than one big thing doing many things badly.

* **Part of the ecosystem**: All work must be done to contribute to and support the existing ecosystem of technology-supported efforts to defend human rights and protect civic engagement.

* **Documentation**: Every technology and process must be documented in a way that makes third-party use as easy as possible.

* **Packaging**: All component parts of the CDR Link stack will be packaged in a way that makes third-party deployment as easy as possible.

## Using CDR Link

We provide a demo for use by potential partners. Log in and usage information is below.

### Logging in

URL: https://demo.digiresilience.org

User: demo@digiresilience.org

Password: Ask us

**Note**: This is a shared demo instance, for people interested in setting up a CDR Link helpdesk. Any data submitted via Signal, Telegram, SMS, email, voice mail, and beyond is visible to others -- including the phone numbers and email addresses used to submit tickets. *Make sure any devices and/or accounts you use to test this platform are not leaking sensitive or mission critical data.*

Finally, dedicated instances of Link will have two-factor authentication set up in the form of a [Keycloak](https://www.keycloak.org), an open-source identity and access management application.

### Ticket creation

Tickets are created when messages are sent via the following channels:

- SMS, Signal, voice recording: +1 480 405 4910
- Telegram: linkdemo_bot
- Email: demo@digiresilience.org
- WhatsApp: +1 646 422 9653

Options also exist to add web forms, chat bots, Twitter @ replies and DMs, and Facebook. Please let us know if you'd like to discuss adding these channels.

You can also create a ticket by clicking the “plus” sign at the bottom left corner.  

![Plus Sign Screenshot](images/plussign.png "Plus Sign Screenshot")

### Deleting tickets

It is easy to delete tickets from the database. Select the desired ticket, and then on the right-hand side of the page, tag a ticket with "delete," and a background task running once every minute will delete any tickets with that tag.  

![Delete Screenshot](images/delete.png "Delete Screenshot")

In addition, all closed tickets will be deleted every 24 hours.

### Users

Users are created or updated when tickets are submitted. If a phone number, email address, or Telegram username is new to the system, that data will be used to create a new user.

You can access a user’s profile in *Settings > Manage > Users*. You can add information like name, email address, phone/Signal/WhatsApp number, and more. New tickets will be associated with existing user data; for instance, messages sent from a Signal number associated with an existing user profile will be added to that user’s open ticket, or, if no open ticket exist, will be used to create a new ticket. New tickets will be used to create new users.

### Usage details

All of these channels (except WhatsApp) work with the demo out of the box.

Try sending messages via the Signal/phone number, Telegram bot, and email. You'll see them arrive in Zammad shortly after sending (in our experience Signal takes 15-30 seconds to come through).

#### Replies

To reply to tickets, click "reply" below the ticket to reply via the original channel (Signal, Telegram, email, etc.). Direct replies to ticket submissions are displayed in a plain white box.

To leave a note under a ticket (only visible by "Agents"), click the lock icon on the left-hand side. This will add an orange outline to the text box, indicating that the note is internal to the Link platform and not being shared with outside users.

**Important note**: The lock iconography can be confusing. The lock does *not* indicate whether a message is encrypted or unencrypted. Rather, the **unlocked** icon means that the note *will* be shared with the user who submitted the ticket. The **locked** icon means that the note *will not* be shared with the user.  

![Lock Notation](images/internalexternal.png "Lock Notation Screenshot")

#### Account permissions

- Admin: View all tickets, add and remove users, change ticket states, add owners  
- Agents: View all tickets, change ticket states, add owners
- Users: View their own tickets. Default permissions assigned to new accounts.

#### Ticket data

By default, three fields appear on the right of a ticket (though this can be customized):

- Owner: Agents and Admins can assign "owners," or other agents, to tickets
- State: Closed, open, pending close, pending reminder. Selecting the last two will create new fields in which you can define when a ticket should close, or when you should be reminded about it.
- Priority: Set the priority level. This can be viewed and filtered in the "Overviews" section.

#### Dashboard, overviews, and reporting

- The **dashboard** (top left of the page) provides an overview of topline stats, including number of tickets assigned, tickets in process, escalated tickets, and more.
- **Overviews** (also top left of the page) provide a global view of all tickets (for admins and agents), including open, pending, assigned, and escalated tickets. Escalation occurs when tickets have been open and unaddressed over a period of time.
- **Reporting** (bottom left) can be useful to analyze trends and to provide stats for external reporting.  

#### Security Consideration: Database 

All CDR Link data is stored on Greenhost servers located in the Netherlands. All network transfers are encrypted in transit using TSL. We don't yet encrypt the servers themselves, but given that our servers are always online -- and therefore would always be decrypted unless turned off -- we believe that in-transit encryption is much more important. We also make daily, encrypted backups of all Link data using the [Tarsnap](https://tarsnap.com) serviceon Greenhost servers located in the Netherlands. All network transfers are encrypted in transit using TSL. We don't yet encrypt the servers themselves, but given that our servers are always online -- and therefore would always be decrypted unless turned off -- we believe that in-transit encryption is much more important. We also make daily, encrypted backups of all Link data using the [Tarsnap](https://tarsnap.com) service.  

#### Security Consideration: Notifications

Notifications can be great to get alerted to a new ticket that requires attention. However, given the integrations of end-to-end encrypted mediums, such as Signal, something coming in via a more secure means may not want to be forwarded along to a less secure one. With email notifications enabled, a message received from Signal could be relayed to an email, thus breaking the end-to-end encryption Signal affords.  

Users can configure where and how they receive notifications by clicking on their avatar in the lower left corner of the window and selecting, "profile". From there, select, "Notifications" and select the appropriate checkboxes in the, "Also notify via email" column.  

![Notifications](images/notifications.png "Notifications")

### Contact

There are many, many other Zammad features, including custom form fields, automatic triggers based on pre-set rules, tags, sorting users into “organizations,” and much more. Zammad has a wealth of their own great documentation, located [here](https://user-docs.zammad.org/en/latest/)

For more details about the CDR Link project, see the [CDR Tech page](https://digiresilience.org/tech). The best way to stay up to date about CDR Link development and other CDR tech projects is to subscribe to our mailing list, using the form on that page. (FYI, we wrote a simple way to accept email signups using Signal, to keep data out of the hands of any third parties. Source code is [here](https://gitlab.com/digiresilience/link/newsletter-sink).) 

Any questions? Contact Josh Levy: josh@digiresilience.org
